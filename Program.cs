﻿﻿using System;

namespace Shapes
{
    class Program
    {
        static void Main(string[] args)
        {
            draw(args[0]);
        }

        public static int Row;
        public static int Column;

        static void WriteAt(string s, int x, int y) {
            Console.SetCursorPosition(Row + y, Column + x);
            Console.Write(s);
        }

        static void draw(string s) {
            s = s.Trim();
            string symbol = $"{s}";

            for (int i = 0; i < 20; i++) {
                Console.WriteLine();
            }

            Console.Clear();
            
            // Rhombus
            Row = Console.CursorTop;
            Column = Console.CursorLeft;

            WriteAt(symbol, 0, 9);

            WriteAt(symbol, 1, 8);
            WriteAt(symbol, 1, 10);

            WriteAt(symbol, 2, 7);
            WriteAt(symbol, 2, 11);

            WriteAt(symbol, 3, 6);
            WriteAt(symbol, 3, 12);

            WriteAt(symbol, 4, 5);
            WriteAt(symbol, 4, 13);

            WriteAt(symbol, 5, 4);
            WriteAt(symbol, 5, 14);

            WriteAt(symbol, 6, 5);
            WriteAt(symbol, 6, 13);

            WriteAt(symbol, 7, 6);
            WriteAt(symbol, 7, 12);

            WriteAt(symbol, 8, 7);
            WriteAt(symbol, 8, 11);

            WriteAt(symbol, 9, 8);
            WriteAt(symbol, 9, 10);

            WriteAt(symbol, 10, 9);

            Console.WriteLine();
            Console.WriteLine();

            // Square
            string hLine = "";
            string vLine = $"{s}                 {s}";
            
            for (int i = 0; i < 10; i++) {
                hLine = hLine + symbol + " ";
            }

            Console.WriteLine(hLine);
            for (int i = 0; i < (10 - 2); i++) {
                Console.WriteLine(vLine);
            }
            Console.WriteLine(hLine);

            Console.WriteLine();
        }
    }
}
